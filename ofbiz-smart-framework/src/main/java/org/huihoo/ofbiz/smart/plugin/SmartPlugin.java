package org.huihoo.ofbiz.smart.plugin;

/**
 * <p>
 * 插件接口
 * </p>
 * 
 * @author huangbaihua
 * @version 1.0
 * @since 1.0
 *
 */
public interface SmartPlugin {
  /**
   * <p>
   * 获取插件的名称
   * </p>
   * 
   * @return
   */
  public String name();

  /**
   * <p>
   * 启动插件
   * </p>
   */
  public void start();

  /**
   * <p>
   * 停止插件
   * </p>
   */
  public void stop();

  /**
   * <p>
   * 插件是否启用
   * </p>
   * 
   * @return
   */
  public boolean isActive();

}
